#!/bin/sh

mkdir ./tmp

wget http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar -O ./tmp/swagger-codegen-cli.jar


java -jar ./tmp/swagger-codegen-cli.jar generate \
   -i /home/e049647/techU/git/techubank/techubank-api/ws-doc/swagger.v2.yaml \
   -l javascript \
   -o ./tmp/techubank-api-client
   -n techubank-api-client

java -jar ./tmp/swagger-codegen-cli.jar generate \
  -i /home/e049647/techU/git/techubank/techubank-api/ws-doc/swagger.v2.yaml \
  -l nodejs-server \
  -o ./tmp/nodejs_server_stub