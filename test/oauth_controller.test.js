/* global describe it afterEach */
const assert = require('assert'),
    {
        mongooseUtils
    } = require('techubank-commons'),
    OAuthController = require('../src/api/controller/oauth_controller.class'),
    sinon = require('sinon'),
    moment = require('moment'),
    jwt = require('jsonwebtoken')


describe('Get token', function () {

    afterEach(() => {
        // Restore the default sandbox here
        sinon.restore()
    })

    it('Create a new token successfully return statusCode 200 and the saved token', function (done) {
        const stub = sinon.stub(mongooseUtils, 'save'),
            user_found = require('./resources/user.json'),
            token_mocked = {
                '_id': {
                    '$oid': '5bf72f730d2aa786e5fdb77e'
                },
                'access_token': '0813b6fbc199a86ab64646b6049c6cee676fec44d9e4f51a34f3de20938d648c22d7b2cf71|de915ff12fb39b1427d2d3c00a6ea0d0|a6e4d2aa38b1b0cb7a3ebe43e72c3ee81fcdde3ec3606ebe2ad670d4bd60cf77',
                'token_type': 'Bearer',
                'scope': 'admin',
                'created': {
                    '$date': '2018-11-22T22:36:35.519Z'
                },
                'expires_in': 86400,
                'username': 'techubank',
                'name': 'techubank',
                'nif': '12345676y',
                '__v': 0
            },
            req = {
                query: {
                    scope: 'admin',
                    state: 'sdsfasf',
                    redirect_uri: 'http://localhost:8001'
                },
                body: {
                    code: 'sdfasfsdfsdf'
                }
            },
            res = {
                setHeader: (key, value) => {
                    return {
                        key,
                        value
                    }
                },
                send: (message) => message,
                statusCode: 200
            },
            mongoose_return = new Promise((resolve) => resolve(token_mocked))

        stub.returns(mongoose_return)

        const oauth_controller = new OAuthController()

        oauth_controller._new_token(user_found, req, res).then((res_code) => {
            assert.equal(res_code, 200)
            done()
        })

    })

    it('When fails to save a new token in Mongo return statusCode 400 return error message', function (done) {
        const stub = sinon.stub(mongooseUtils, 'save'),
            new_token = {
                'access_token': '0813b6fbc199a86ab64646b6049c6cee676fec44d9e4f51a34f3de20938d648c22d7b2cf71|de915ff12fb39b1427d2d3c00a6ea0d0|a6e4d2aa38b1b0cb7a3ebe43e72c3ee81fcdde3ec3606ebe2ad670d4bd60cf77',
                'token_type': 'Bearer',
                'scope': 'admin',
                'created': {
                    '$date': '2018-11-22T22:36:35.519Z'
                },
                'expires_in': 86400,
                'username': 'techubank',
                'name': 'techubank',
                'nif': '12345676y'
            },
            req = {
                query: {
                    scope: 'admin',
                    state: 'sdsfasf',
                    redirect_uri: 'http://localhost:8001'
                },
                body: {
                    code: 'sdfasfsdfsdf'
                }
            },
            res = {
                setHeader: (key, value) => { return { key: value } },
                send: (message) => message,
                statusCode: 200
            }, mongoose_return = new Promise((resolve, reject) => reject('TEST Error saving in mongo'))
        stub.returns(mongoose_return)

        const oauth_controller = new OAuthController()

        oauth_controller._new_token(new_token, req, res).then(() => {
            done()
        }).catch((err_code) => {
            assert.equal(err_code, 400)
            done()
        })

    })

    it('JWT validate', function (done) {
        const jwt_token = {
                scope: 'user',
                username: 'dummy',
                name: 'Dummy',
                nif: '12345678Y'
            },
            access_token = jwt.sign(jwt_token, 'YOU ARE GRANTED TO ACCESS THE API!!!!', { expiresIn: '1h' })

        assert.equal(jwt.verify(access_token, 'YOU ARE GRANTED TO ACCESS THE API!!!!').username, jwt_token.username)
        done()
    })

    it('JWT check expires session', function (done) {
        const jwt_token = {
                scope: 'user',
                username: 'dummy',
                name: 'Dummy',
                nif: '12345678Y'
            },
            access_token = jwt.sign(jwt_token, 'YOU ARE GRANTED TO ACCESS THE API!!!!', { expiresIn: '1s' })

        setTimeout(() => {
            try {
                jwt.verify(access_token, 'YOU ARE GRANTED TO ACCESS THE API!!!!')
            } catch(error){
                done()
            }   
        }, 1100)
    })
    /*     it('Happy path - Admin user techubank get a token with admin role', function () { */
    /* 
            let stub = sinon.stub(mongooseUtils, 'findOne')

            stub.returns(new Promise((resolve,reject) => resolve({
                scopes: ['admin'],
                _id: '5bf5c8b8de7d662bc1c78d68',
                username: 'techubank',
                name: 'techubank',
                first_name: 'techubank',
                last_name: 'techubank',
                nif: '12345676y',
                birth_date: '01/02/2019',
                phone: '+34911234567',
                email: 'd.d@d.es',
                password: 'f54c40c02cc861d619ad7d89d9|1a2490bd8f45b812da03ed47022e83a7|9c6c7617ed2b47cd4bf6be516663e99ae577d5dad9436a0941c04a6d1ff4aa8f',
                address:
                {
                    _id: '5bf5c8b8de7d662bc1c78d69',
                    street: 'techubank',
                    zip_code: 'techubank',
                    city: 'techubank',
                    country: 'techubank'
                },
                register_date: '2018 - 11 - 21T21: 06: 00.264Z',
                status: 'ACTIVE',
                __v: 0
            })))

     */
    /*         const req = {
                body: {
                    client_id: 'techubank',
                    client_secret: 'techubank1234',
                    grant_type: 'authorization_code',
                    code: 'dummycode',
                    redirect_uri: 'http://localhost:8080'
                },
                query: {
                    scope: 'admin',
                    state: '1234567890'
                }
            },
                res = {
                    send: (data) => {
                        console.log(data)
                    },
                    setHeader: (...args) => {
                        console.log(args)
                    },
                    statusCode: 0
                }
            oauth.get_token(req, res)

            assert(res.statusCode == 200, "User techubank is allowed to get admin grants")
        })

        it('Wrong arguments returns error 400', function () {
        })

        it('Invalid credentials returns 403', function () {
        }) */


})