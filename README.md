# TechU Bank OAuth Server
Light OAuth2 Server for training purposes.

This project implementes the [OAuth2 accessCode flow](https://www.oauth.com/oauth2-servers/access-tokens/authorization-code-request/).

>
> [Understanding OAuth2 and Building a Basic Authorization Server of Your Own: A Beginner’s Guide](https://medium.com/google-cloud/understanding-oauth2-and-building-a-basic-authorization-server-of-your-own-a-beginners-guide-cf7451a16f66) 
> Control sessions via [JWT tokens](https://jwt.io/introduction/)
>

# Features
- Supports /authorize and /token endpoints
- Validation Authorities (VA) can check if tokens and scopes are valid
- Persist JWT tokens in a MongoDB instance
- Validate user credentials against a MongoDB collection (users)
- Remove tokens (logout)

# API Documentation

The API documentation is written following the Open API v2 specification in a Swagger YAML file.

You can find all the specification versions in ws-doc folder. It's automatically rendered using swagger-ui library.

If you want to check the documentation and try any endpoint (it only works in localhost):

1) Start the nodejs server
```
$ npm install
$ npm run start

```
2) Access the [ws-doc link|http://localhost:4000/ws-doc] 
3) You will be able to test the endpoints setting the apki-key variable with the value "test"


# Quick Start

Create a *users* collection in a MongoDB instance. You can use [mlab](https://mlab.com/login/?r=%2Fdatabases%2Ftechubank-jcmdrv) for testing purposes.

```
// The following example has set the password to "secret"
{
	"username": "sagan",
	"first_name": "Peter",
	"last_name": "Sagan",
	"nif": "12345678Y",
	"birth_date":  {
        "$date": "1978-10-24T08:55:09.820Z"
    },
	"phone": "+34 666-66-66-66",
	"email": "user@email.com",
	"password": "$2b$10$uT37t75jAruP3ZTqnx8eMu0aGtjwK4qX7pXs4kn9IOw4tEYXncn1K",
	"status": "ACTIVE",
	"logged": false,
	"register_date":  {
        "$date": "2018-10-24T08:55:09.820Z"
    },
	"user_status": "ACTIVE",
	"address": {
		"street": "Bike Avenue",
		"zip_code": "496978",
		"city": "Dublin",
		"country": "Ireland"
	}
}
```

Create a *.env* file on the base path with MongoDB credentials
```
MLAB_HOST=<mongo-host>
MLAB_DATABASENAME=<mongo-databasename>
MLAB_USERNAME=<username>
MLAB_PASSWORD=<pwd>
```

Install dependencies and launch the server
```
$ npm install

$ npm run start
```

```
$ npm run start

[nodemon] 1.18.4
[nodemon] to restart at any time, enter `rs`
[nodemon] watching: *.*
[nodemon] starting `node src/index.js`
info: ************************
info: Logger initialized
info: ************************
info: App listening on port 4000!
```

# Development

## Requirements
This project needs:
- Nodejs 8.4+
- Docker 17+ if you want to publish a new docker image or run the latest TechU Bank docker image
- Access to a MongoDB instace with a valid *users* collection.

## Folders

- bin: helpers
	- pre-commit: execute linter and unit tests before a commit
	- swagger-codegen.sh: parse swagger.vX.yaml and generates an api-client and a scaffold for the server side.
- src: The API logic
- ws-doc: Here you can find the swager.v1.yml with API's documentation
- test: unit tests folder


## Compilation
Stay on the base path where package.json is located.

```
$ npm install


If you want to update packages

$ npm update

```

## Unit tests

1) Stay on the base path where package.json is located.

```
techu@techu> npm run test

```

2) If you want to execute unit tests on each change, execute the watcher command.
```
techu@techu> npm run watcher

```

3) To get coverage metrics run Instanbul
```
techu@techu> npm run coverage

```

# Static code analysis

## Linter
Linter is a light weight static code analysis recomended to be execute in every push to a branch, and may block the push.
```
techu@techu> npm run lint

```

## Sonarqube - sonar-scanner Docker image
We have prepared a container with the software enough for run a static code analysis, execute unit tests, measure unit test coverage and upload the results to Sonarqube (free account). 

You must have a Sonarqube API Key, configure properly the sonar-scanner.properties and run the command below:
```
$ docker run -it --env SONAR_API_KEY=<SONAR_USER_API_KEY> --mount type=bind,source="$(pwd)",target=/root/project -v "$(pwd)"/sonar-scanner.properties:/root/sonar-scanner/conf/sonar-scanner.properties davidrova/sonar-scanner:latest
```

To see the result [click on this link](https://sonarcloud.io/projects).

>
> The evaluation period has expired and we cannot upload more executions.
>

Don't worry, you can still perform static code analysis, but following a couple of additional steps.

## Sonarqube - sonar Docker image

1) Launch a sonarqube instance (admin / admin)
```
$ docker run -d -p 9000:9000 -p 9092:9092 sonarqube
```

2) Generate a new API-KEY

3) Download sonar-scanner in localhost or use sonar-scanner Docker image
    [sonar-scanner documentation](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner)

4) Run unit tests and coverage
```
$ npm run test
$ npm run coverage
```

5) Run sonar-scanner; sonar-scanner loads sonar-project.properties file
```
$ SONAR_API_KEY=<SONAR_API_KEY> sonar-scanner
```

6) View the results in [Sonarqube](http://localhost:9000)



  

## Hooks
We encourage to enable the pre-commit hook. It will avoid commiting a change if linter or unit tests fail.

```
$ cp bin/pre-commit .git/hooks/pre-commit

```

## Continuous integration
The project is managed in bitbucket.org and we have automated an ease pipeline with the following step rules:

- A commint into *develop* branch triggers: 
	- build
	- lint
	- unit tests
	- sonar
	- Increases the *patch version*
	- Publish the artifact to npmrc registry
	- Publish a new docker image into Docker Hub
- A commit into *master* branch triggers:
	- build
	- lint
	- unit tests
	- sonar
	- Increases the *minor version*
	- Publish the artifact to npmrc registry
	- Publish a new docker image into Docker Hub


NOTE: It's neccessary to generate a new key pair. [Follow these instructions](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html?_ga=2.62235695.2086604014.1539165652-1490061868.1526909294)


# TechU Bank OAuth Server Docker image

# Start TechU Bank OAuth Server docker image
```
docker run -it -p 4000:4000 -p 4443:4443 -e PORT=4000 -e PORT_SSL=4443 -e MLAB_HOST=<host>:<port> -e MLAB_DATABASENAME=<databasename> -e MLAB_USERNAME=<mlab-username> -e MLAB_PASSWORD=<mlab-password> -e ENV=docker ${DOCKER_HUB_USERNAME}/techubank-auth-server
```

# Stop TechU Bank OAuth Server docker image
```
docker stop techubank__oauth__server__testing
```

# Remove TechU Bank OAuth Server docker image
```
docker rm techubank__oauth__server__testing
```

# Building and push the docker image

```
docker build -t ${DOCKER_HUB_USERNAME}/techubank-oauth-server .
docker login --username=${DOCKER_HUB_USERNAME} --password=${DOCKER_HUB_PASSWORD}
docker push ${DOCKER_HUB_USERNAME}/techubank-oauth-server 
```

# Setup https

Create an self-signed SSL certificate

* Write down the Common Name (CN) for your SSL Certificate. The CN is the fully qualified name for the system that uses the certificate. If you are using Dynamic DNS, your CN should have a wild-card, for example: *.api.com. Otherwise, use the hostname or IP address set in your Gateway Cluster (for example. 192.16.183.131 or dp1.acme.com).

* Run the following OpenSSL command to generate your private key and public certificate. Answer the questions and enter the Common Name when prompted.

```
$ openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out certificate.pem


Country Name (2 letter code) [AU]:localhost
string is too long, it needs to be no more than 2 bytes long
Country Name (2 letter code) [AU]:ES
State or Province Name (full name) [Some-State]:MADRID
Locality Name (eg, city) []:MADRID
Organization Name (eg, company) [Internet Widgits Pty Ltd]:techubank
Organizational Unit Name (eg, section) []:techubank
Common Name (e.g. server FQDN or YOUR name) []:techubank
Email Address []:techubank@techu.com

```
* key.pem is the private key
* certificate.pem is the self-signed certificate

* Copy the pem files on the basepath 

```
$ techubank-auth-server > ls -lrt
drwxrwxr-x   2 techu techu    4096 oct 16 10:43 bin
-rw-rw-r--   1 techu techu     180 oct 19 08:28 Dockerfile
-rw-r--r--   1 techu techu     680 oct 30 10:32 sonar-scanner.properties
drwxrwxr-x   2 techu techu    4096 oct 30 16:24 test
-rw-rw-r--   1 techu techu    2396 nov  6 09:26 bitbucket-pipelines.yml
drwxrwxr-x   3 techu techu    4096 nov 12 07:59 src
-rw-rw-r--   1 techu techu    1633 nov 12 07:59 package.json
drwxrwxr-x   2 techu techu    4096 nov 12 07:59 ws-doc
-rw-------   1 techu techu    1708 nov 12 09:50 key.pem
-rw-rw-r--   1 techu techu    1436 nov 12 09:51 certificate.pem
drwxrwxr-x 523 techu techu   20480 nov 12 09:53 node_modules
-rw-rw-r--   1 techu techu    6483 nov 12 09:54 README.md

```

References: 
* https://www.sitepoint.com/how-to-use-ssltls-with-node-js/
* https://www.ibm.com/support/knowledgecenter/en/SSWHYP_4.0.0/com.ibm.apimgmt.cmc.doc/task_apionprem_gernerate_self_signed_openSSL.html
