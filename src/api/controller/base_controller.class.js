const requestJson = require('request-json'),
    {
        environment,
        logger,
        get_admin_token
    } = require('techubank-commons'),
    client = requestJson.createClient(`${environment.value.techubank_mlab_url}`),
    oauth_client = requestJson.createClient(`${environment.value.techubank_auth_server_url}`)



module.exports = class BaseController {
    constructor() {
        this.client = client
        this.oauth_client = oauth_client
    }

    static options(req, res) {
        return new Promise((resolve) => {
            res.statusCode = 200
            res.send('OK')
            resolve('OK')
        })
    }

    load_token() {
        return get_admin_token().then((token) => {
            logger.info(`Admin token fetched ${JSON.stringify(token)}`)

            this.client.headers['authorization'] = token
            this.oauth_client.headers['authorization'] = oauth_client
        }).catch(() => {
            logger.error('Fail getting token')
        })
    }


    client_get(url) {
        return new Promise((resolve, reject) => {
            return this.client.get(url, (err, res) => {
                if (err)
                    return reject(err)
                return resolve(res)
            })
        })
    }

    client_put(url, body) {
        return new Promise((resolve, reject) => {
            return this.client.put(url, body, (err, res) => {
                if (err)
                    return reject(err)
                return resolve(res)
            })
        })
    }

    client_post(url, body) {
        return new Promise((resolve, reject) => {
            return this.client.post(url, body, (err, res) => {
                if (err){
                    logger.error(`client_post: ${err}`)
                    return reject(err)
                }
                return resolve(res)
            })
        })
    }

    /**
     * This method replaces techubank-commons.http_request_handler when you return a Promise instead a HTTP Response
     * 
     * @param {object} res This is the response object to be sent to the requestor. This is the object to use to finish the call.
     * @param {object} handlers A json with all the possible handlers managed by our application (error 200, 400, 403, 401, 500). Let's stablish a naming convention for those function names ( methodName_error_handler, ....)
     * 	{
     *	success_handler: methodName_success_handler,
     *	error_handler: methodName_error_handler,
     *	unauthorized_handler: methodName_unauthorized_handler,
     *	forbidden_handler: methodName_forbidden_handler,
     *	server_error_handler: methodName_server_error_handler
     * }
     * @param {object} callback This is the error object receive by an incorrect http request
     * @param {object} ...additional_args Aditional arguments
     */
    request_handler(res, handlers, callback, ...additional_args) {
        switch (res.statusCode) {
        case 200:
            return handlers.success_handler.call(this, res, callback, ...additional_args)
        case 400:
            return handlers.error_handler.call(this, res, callback, ...additional_args)
        case 401:
            return handlers.unauthorized_handler.call(this, res, callback, ...additional_args)
        case 403:
            return handlers.forbidden_handler.call(this, res, callback, ...additional_args)
        case 404:
            return handlers.notfound_handler.call(this, res, callback, ...additional_args)
        case 500:
            return handlers.server_error_handler.call(this, res, callback, ...additional_args)
        default:
            return callback.send('statusCode not managed')
        }
    }

}