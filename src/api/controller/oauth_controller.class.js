const jwt = require('jsonwebtoken'),
    {
        logger,
        crypt,
        mongooseUtils,
        environment
    } = require('techubank-commons'),
    {
        user
    } = require('../models/user'),
    {
        token
    } = require('../models/token'),
    BaseController = require('./base_controller.class')

module.exports = class OAuthController extends BaseController {
    constructor() {
        super()
    }

    static authorize(req, res) {
        const account_controller = new OAuthController()
        return account_controller._authorize(req, res)
    }

    static get_token(req, res) {
        const account_controller = new OAuthController()
        return account_controller._get_token(req, res)
    }

    static redirect(req, res) {
        const account_controller = new OAuthController()
        return account_controller._redirect(req, res)
    }

    _authorize(req, res) {
        logger.info('################# /authorize: Requesting authorization (QUERY) #################')
        logger.info(JSON.stringify(req.query))
        logger.info('################# /authorize: Requesting authorization (HEADERS) #################')
        logger.info(JSON.stringify(req.headers))
        logger.info('################# /authorize: Requesting authorization (BODY) #################')
        logger.info(JSON.stringify(req.body))
        logger.info('################# /authorize: SESSION #################')
        logger.info(JSON.stringify(req.session))

        const redirect = (req.query['redirect_uri']) ? req.query['redirect_uri'] : `${environment.value.techubank_auth_server_url}/oauth/redirect`

        res.redirect(`${redirect}?state=${req.query['state']}&scope=${req.query['scope']}&code=${crypt.hash(req.query['state'])}`)
    }

    _get_token(req, res) {
        logger.info('################# /token: Requesting token (QUERY) #################')
        logger.info(JSON.stringify(req.query))
        logger.info('################# /token: Requesting token (HEADERS) #################')
        logger.info(JSON.stringify(req.headers))
        logger.info('################# /token: Requesting token (BODY) #################')
        logger.info(JSON.stringify(req.body))

        const filter = {
            $or: [{
                username: req.body['client_id']
            }, {
                email: req.body['client_id']
            }, {
                nif: req.body['client_id']
            }]
        }

        return mongooseUtils.findOne(user, filter).then((user_found) => {
            req.query['scope'] = (req.query['scope']) ? req.query['scope'] : 'user'
            if (crypt.checkpassword(req.body['client_secret'], user_found.password) && (user_found['scopes'].indexOf(req.query['scope']) >= 0)) {
                return this._get_token_from_store(user_found, req, res)
            } else {
                res.statusCode = 401
                return res.send('Unauthorized')
            }
        }).catch((notfound) => {
            res.statusCode = 404
            return res.send(`User not found: ${notfound}`)
        })
    }

    _redirect(req, res) {
        res.send({
            code: req.query['code'],
            state: req.query['state']
        })
    }

    /**
     * 
     */
    _get_token_from_store(user_found, req, res) {
        logger.info('_get_token_from_store')

        if (req.query.access_token) {
            return this._try_to_reuse_token(req, res)
        } else {
            return this._new_token(user_found, req, res)
        }
    }

    /**
     * Fin a token in MongoDB collection and returns it.
     * Otherwise it creates a new token and returns it
     */
    _try_to_reuse_token(req, res) {
        return mongooseUtils.findOne(token, {
            access_token: req.query.access_token
        }).then((token_found) => {
            logger.info(`Token found: ${token_found}`)
            if (token_found) {
                return this._check_if_token_valid(token_found, req, res)
            } else {
                return this._new_token(req, res)
            }
        }).catch((err) => {
            logger.err(`Token not found: ${err}`)
            res.statusCode = 404
            res.send(err)
        })
    }
    /**
     *  It token is still valid returns it again.
     */
    _check_if_token_valid(token_found, req, res) {
        if (this._is_token_still_valid(token_found)) {
            logger.info('Token is still valid')
            return res.send(token_found)
        } else {
            logger.info('Token expired.')
            res.statusCode = 401
            res.send('token expired')
            return mongooseUtils.delete(token, {
                access_token: req.query.access_token
            }).then(() => {
                logger.info('Token expired deleted in MongoDB')
            }).catch((error) => {
                logger.error('Error deleting old token')
                res.statusCode = 500
                res.send(error)
            })
        }

    }

    /**
     * 
     * @param {*} token_found 
     */
    _is_token_still_valid(token_found) {
        try {
            jwt.verify(token_found, 'YOU ARE GRANTED TO ACCESS THE API!!!!')
            return true
        } catch(error){
            return false
        }  

/*        
        const token_moment = moment(new Date(token_found.created.$date))

        token_moment.add(token_found.expires_in, 'seconds')
        if (!token_moment)
            return false
        return new Date() > token_moment 
*/
    }


    /**
     * Save the token into MongoDB and send the response to the client
     * @param {*} user_found 
     * @param {*} req 
     * @param {*} res 
     */
    _new_token(user_found, req, res) {

        const new_token = new token()

        //new_token.access_token = crypt.hash('YOU ARE GRANTED TO ACCESS THE API!!!!')
        new_token.token_type = 'Bearer'
        new_token.code = req.body['code']
        new_token.scope = (req.query['scope']) ? req.query['scope'] : 'user'
        new_token.state = req.query['state']
        new_token.redirect_uri = req.query['redirect_uri']
        new_token.created = new Date()
        new_token.username = user_found.username
        new_token.name = user_found.name
        new_token.nif = user_found.nif

        const jwt_token = {
            scope: new_token.scope,
            username: user_found.username,
            name: user_found.name,
            nif: user_found.nif
        }
        new_token.access_token = jwt.sign(jwt_token, 'YOU ARE GRANTED TO ACCESS THE API!!!!',  { expiresIn: '1h' })

        logger.info(`Storing a new token: ${new_token.access_token}`)

        return new Promise((resolve, reject) => {
            return mongooseUtils.save(new_token).then((token_saved) => {
                logger.debug(`Token saved in MongoDB: ${token_saved}`)
                res.setHeader('Cache-Control', 'no-store')
                res.setHeader('Pragma', 'no-cache')
                res.statusCode = 200
                res.send(token_saved)
                return resolve(res.statusCode)
            }).catch((err) => {
                logger.error(`Error storing the new token: ${err}`)
                res.statusCode = 400
                res.send(err)
                return reject(res.statusCode)
            })
        })
    }
}