const {
        logger,
        mongooseUtils
    } = require('techubank-commons'),
    {
        token
    } = require('../models/token'),
    BaseController = require('./base_controller.class')

module.exports = class UserController extends BaseController {
    constructor() {
        super()
    }

    static logout(req, res) {
        const user_controller = new UserController()

        user_controller._logout(req, res)
    }

    _logout(req, res) {
        logger.info('################# /logout: Requesting token (HEADERS) #################')
        logger.info(JSON.stringify(req.headers))

        const access_token = req.headers.authorization.replace('Bearer', '').trim()

        return mongooseUtils.deleteOne(token, { access_token: access_token }).then((delete_res) => {
            res.send(`User logged out: ${delete_res} `)
        }).catch((err) => res.send(`ERROR logging out: ${err}`))
    }
}

