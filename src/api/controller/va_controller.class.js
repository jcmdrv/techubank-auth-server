const jwt = require('jsonwebtoken'),
    BaseController = require('./base_controller.class')
/* {    logger,
    mongooseUtils
} = require('techubank-commons'), {
    token
} = require('../models/token'),
 */

module.exports = class VAController extends BaseController {
    constructor() {
        super()
    }

    static check_token(req, res) {
        const va_controller = new VAController()

        return va_controller._check_token(req, res)
    }

    _check_token(req, res) {
        try {
            const decoded = jwt.verify(req.body.target_token, 'YOU ARE GRANTED TO ACCESS THE API!!!!')

            if (decoded && decoded.scope === req.body.target_scope) {
                res.send({
                    status: 'ok'
                })
            } else {
                res.statusCode = 403
                res.send({
                    status: 'Your token doesn\'t have privileges'
                })
            }
        } catch(error) {
            res.statusCode = 403
            res.send({
                internal_code: 'invalid-token',
                message: `Error validating token: ${error}`
            })       
        }
    }
}