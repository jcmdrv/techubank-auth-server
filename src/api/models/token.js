const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    TokenSchema = new Schema({
        access_token: {
            type: String,
            required: true
        },
        token_type: {
            type: String,
            required: true
        },
        scope: {
            type: String,
            required: true
        },
        created: {
            type: Date,
            required: true
        },
        username: {
            type: String,
            required: true
        },
        nif: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        }
    })

module.exports.TokenSchema = TokenSchema
module.exports.token = mongoose.model('token', TokenSchema)