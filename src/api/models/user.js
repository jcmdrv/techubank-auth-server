/* eslint no-useless-escape: 0 */  
/**
 * @fileoverview
 * Provides the techubank-mlab user model.
 * 
 */

const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    {AddressSchema} = require('./address'),
    UserSchema = new Schema({
        username: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },		
        first_name: {
            type: String,
            required: true
        },
        last_name: {
            type: String
        },
        nif: {
            type: String,
            required: true
        },
        email: {
            type: String,
            validate: [_validateEmail, 'Please fill a valid email address'],
            required: true
        },
        password: {
            type: String,
            required: true
        },
        phone: {
            type: String,
            required: true
        },
        status: {
            type: String,
            required: true,
            enum: ['ACTIVE', 'BLOCKED']
        },
        logged: {
            type: Boolean
        },
        birth_date: {
            type: String,
            required: true
        },
        register_date: {
            type: Date,
            required: true
        },
        last_login_date: {
            type: Date
        },
        address: AddressSchema,
        scopes: [String]
    })


function _validateEmail (email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

    return re.test(email)
}

UserSchema.index({
    username: 1
}, {
    unique: true
}) 

module.exports.UserSchema = UserSchema
module.exports.user = mongoose.model('user', UserSchema)