const express = require('express'),
    va_router = express.Router(),
    VAController = require('../controller/va_controller.class')


// Add OAuth Security
va_router.use(function (req, res, next) {
    if (!req.headers['authorization']) {
        res.status(401).send('Unauthorized: you must be logged before doing any request')
    } else {            
        next()
    }
})

/**
 * Confidence entities have access to check if a token is valid
 * So, an authenticated user wants to check a token (body)
 */
va_router.post('/check-token', VAController.check_token)


module.exports.va_router = va_router