const express = require('express'),
    user_router = express.Router(),
    UserController = require('../controller/user_controller.class')

// Add OAuth Security
user_router.use(function (req, res, next) {
    if (!req.headers['authorization']) {
        res.status(401).send('Unauthorized: you must be logged before doing any request')
    } else {    
        next()
    }
})

/**
 * Destroy session
 * 
 */
user_router.get('/logout', UserController.logout)


module.exports.user_router = user_router