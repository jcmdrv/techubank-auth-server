const express = require('express'),
    oauth_router = express.Router(),
    OAuthController = require('../controller/oauth_controller.class')

/**
 * Request's query params:
 * - response_type	Required. Must be set to code
 * - client_id	Required. The client identifier as assigned by the authorization server, when the client was registered.
 * - redirect_uri	Optional. The redirect URI registered by the client.
 * - scope	Optional. The possible scope of the request.
 * - state	Optional (recommended). Any client state that needs to be passed on to the client request URI.
 * 
 * Response query params:
 * - code	Required. The authorization code.
 * - state	Required, if present in request. The same value as sent by the client in the state parameter, if any.
 * 
 */
oauth_router.get('/authorize', OAuthController.authorize)

/**
 * Request headers:
 * - client_id	Required. The client application's id.
 * - client_secret	Required. The client application's client secret .
 * - grant_type	Required. Must be set to authorization_code .
 * - code	Required. The authorization code received by the authorization server.
 * - redirect_uri	Required, if the request URI was included in the authorization request. Must be identical then.
 * 
 * Response body:
 * { "access_token"  : "...",
 *   "token_type"    : "...",
 *   "expires_in"    : "...",
 *   "refresh_token" : "...",
 *  }
 */
oauth_router.post('/token', OAuthController.get_token)


/**
 * Get the oauth2 redirection. 
 * 
 */
oauth_router.get('/redirect', OAuthController.redirect)


module.exports.oauth_router = oauth_router