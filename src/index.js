require('dotenv').config()

const pjson = require('../package.json'),
    path = require('path'),
    fs = require('fs'),
    express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    app = express(),
    port = 4000,
    port_ssl = 4443,
    {
        oauth_router
    } = require('./api/router/oauth_router'), {
        va_router
    } = require('./api/router/va_router'), {
        user_router
    } = require('./api/router/user_router'), {
        environment,
        logger,
        enable_cors
    } = require('techubank-commons'),
    mlab_username = process.env.MLAB_USERNAME || environment.value.techubank_mlab_username,
    mlab_password = process.env.MLAB_PASSWORD || environment.value.techubank_mlab_password,
    mlab_host = process.env.MLAB_HOST || environment.value.techubank_mlab_host,
    mlab_databasename = process.env.MLAB_DATABASENAME || environment.value.techubank_mlab_databasename


_connect_mongo()

if (environment.value.enableCors) {
    app.use(enable_cors)
}


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}))

// parse application/json
app.use(bodyParser.json())

// public
app.use('/ws-doc', express.static(path.join(__dirname, '..', 'ws-doc')))
app.use('/oauth', oauth_router)

// secured
app.use('/va', va_router)
app.use('/user', user_router)


// CHECK IF COMMUNICATION IS OVER HTTP OR HTTPS
//if (environment.value.protocol === 'https') {
const https = require('https'),
    helmet = require('helmet')

app.use(helmet()) // Add Helmet as a middleware
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0 // this allows self-signed cer
https.createServer({
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('certificate.pem')
}, app).listen(port_ssl)
//} else {
app.listen(port)
//}

logger.info(`***** techubank-auth-server@${pjson.version} *************`)
logger.info(`App listening HTTP on port ${port}`)
logger.info(`App listening HTTPS on port ${port_ssl}`)

function _connect_mongo() {
    logger.info(`Connection to Mongo DB ${mlab_host}/${mlab_databasename}`)

    mongoose.Promise = global.Promise
    mongoose.connect(`mongodb://${mlab_username}:${mlab_password}@${mlab_host}/${mlab_databasename}`, {
        useNewUrlParser: true,
        autoIndex: true,
        useCreateIndex: true,
        useFindAndModify: false
    })
    mongoose.set('useCreateIndex', true)
}